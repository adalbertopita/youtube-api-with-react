const videoReducer = (state = {
    videos: [],
    actual_country: 'US',
    countries: [
      {ab:"AR", name: "Argentina"},
      {ab:"AU", name: "Australia"},
      {ab:"BR", name: "Brazil"},
      {ab:"CO", name: "Colombia"},
      {ab:"CZ", name: "Czech Republic"},
      {ab:"FR", name: "France"},
      {ab:"DE", name: "Germany"},
      {ab:"GB", name: "Great Britain"},
      {ab:"IT", name: "Italy"},
      {ab:"JP", name: "Japan"},
      {ab:"MX", name: "Mexico"},
      {ab:"NL", name: "Netherlands"},
      {ab:"RU", name: "Russia"},
      {ab:"SA", name: "Saudi Arabia"},
      {ab:"ZA", name: "South Africa"},
      {ab:"KR", name: "South Korea"},
      {ab:"ES", name: "Spain"},
      {ab:"SE", name: "Sweden"},
      {ab:"US", name: "United States"}
    ]
}, action) => {
    switch (action.type) {
        case "GET_FEED":
            state = {
                ...state,
                videos: action.payload
            };
            break;
        case "SET_COUNTRY":
            state = {
                ...state,
                actual_country: action.payload
            };
            break;
        default:
            return state
    }
    return state;
};

export default videoReducer;