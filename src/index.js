import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from "react-router";
import {Provider} from "react-redux";

import './index.css';

import Layout from "./components/layout";
import Videos from "./containers/videos";
import VideoView from "./components/videoView";

import store from "./store";

class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
          <Route path={"/"} component={Layout} >
              <IndexRoute component={Videos} />
              <Route path={"home"} component={Videos} />
              <Route path={"video/:id"} component={VideoView} />
          </Route>
      </Router>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
