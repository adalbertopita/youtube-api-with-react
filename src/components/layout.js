import React, { Component } from 'react';

import Header from "./header";

class Layout extends Component {
  render() {
    return (
    <div>
        <div className="wrapper">
          <div className="container">
            <Header title={"React app"}/>
            {this.props.children}
          </div>
        </div>
    </div>
    );
  }
}

export default Layout;