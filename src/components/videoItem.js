import React from 'react';
import {Link} from "react-router";

const VideoItem = (props) => {
    const video_link = "/video/"+ props.id;
    return (
        <div className="col-sm-4">
            <div className="card card-block">
                <Link to={video_link}>
                    <img className="card-img-top" width="310" height="180" src={props.img} alt="Card cap" />
                </Link>
                <div className="card-block">
                    <h5 className="card-title">{props.title}</h5>
                    <p className="card-text">{props.views} views</p>
                </div>
            </div>
        </div>
    );
}

export default VideoItem