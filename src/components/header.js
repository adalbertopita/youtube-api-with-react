import React from "react";


const Header = (props) => {
    return (
        <div>
            <nav className="navbar navbar-fixed-top navbar-dark bg-inverse">
              <div className="container">
                <a className="navbar-brand" href="#">{props.title}</a>
              </div>
            </nav>
        </div>
    );
}

export default Header