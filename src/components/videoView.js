import React from 'react';

const VideoView = (props) => {
    let video_url = "https://www.youtube.com/embed/" + props.params.id;
    return (
        <div className="row">
            <iframe width="854" height="480" src={video_url} frameBorder="0" allowFullScreen></iframe>
        </div>
    );
}

export default VideoView;