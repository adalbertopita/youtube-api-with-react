import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import videos from "./reducers/videoReducer";

export default createStore(
    combineReducers({
        videos
    }),
    {},
    applyMiddleware(promise(), thunk, logger())
);