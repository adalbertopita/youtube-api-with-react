import axios from "axios";

export function getFeed(country) {
    return function(dispatch){
        let feed_url = "https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics&maxResults=30&chart=mostPopular&key=AIzaSyCUQPvHbxVuFHojEjx7wijXp4HyCGM9Vcc&regionCode="+ country;
        axios.get(feed_url)
        .then((response)=>{
            dispatch({ 
                type: "GET_FEED", 
                payload: response.data.items
            });

            dispatch({
                type: "SET_COUNTRY",
                payload: country
            })
        })
        .catch((err)=> {
            console.log('erro: ', err)
        })      
    };
}