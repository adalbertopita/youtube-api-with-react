import React, { Component } from 'react';
import {connect} from "react-redux";

import { getFeed } from "../actions/videoActions";

class Filter extends Component {
  
  handleChange(event){
      let selectedCountry = event.target.value;
      this.props.getFeed(selectedCountry);
  }

  render() {
    return (
    <div>
        <select className="custom-select" defaultValue={this.props.actual_country} onChange={this.handleChange.bind(this)}>
            <option>Select a country</option>
            {this.props.countries.map((country) => <option value={country.ab}>{country.name}</option>)}
        </select>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      countries: state.videos.countries,
      actual_country: state.videos.actual_country
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFeed: (country) => {
            dispatch(getFeed(country));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);