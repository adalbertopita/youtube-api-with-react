import React, { Component } from 'react';
import {connect} from "react-redux";

import Filter from "./filter";

import VideoItem from "../components/videoItem";
import { getFeed } from "../actions/videoActions";

class Videos extends Component {
    componentDidMount() {
        this.props.getFeed(this.props.actual_country)
    }
    
    render() {
        const { videos } = this.props;
        return (
            <div>
                <Filter />
                <hr/>
                <div className="row">
                    {videos.map((vid) => <VideoItem 
                                            img={vid.snippet.thumbnails.medium.url} 
                                            views={vid.statistics.viewCount} 
                                            key={vid.id} 
                                            id={vid.id} 
                                            title={vid.snippet.title} 
                                        />
                                )}
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
  return {
      videos: state.videos.videos,
      actual_country: state.videos.actual_country
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFeed: (country) => {
            dispatch(getFeed(country));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Videos);